const readline = require("readline");

const { flipper } = require("./flipper");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const askQuestion = question => {
  return new Promise(resolve => {
    rl.question(question, resolve);
  });
};

return askQuestion("How many pancake stacks would you like to flip? ")
  .then(stacks => {
    const n = Number.parseInt(stacks);
    if (n > 100) {
      throw new Error("Whoops - that's out of bounds!");
    }
    return Array(n)
      .fill(0)
      .reduce(chain => {
        return chain.then(samples =>
          askQuestion("").then(result => samples.concat([result]))
        );
      }, Promise.resolve([]));
  })
  .then(samples => samples.map(flipper))
  .then(results => {
    results.forEach((result, index) => {
      console.log(`Case #${index + 1}: ${result}`);
    });
    rl.close();
  })
  .catch(reason => {
    console.error("Oops! Something went wrong.");
    console.error(reason);
    rl.close();
  });

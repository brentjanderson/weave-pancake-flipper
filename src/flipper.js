/**
 * Core calling function to kick the process off
 * Takes a string of plusses and minuses, returns how many flips it takes to get them all into plusses
 * @param {String} rawCase String of only + or -
 */
const flipper = rawCase => {
  // 1. Convert string to array
  // Happy pancakes are true, blank pancakes are false
  const workingCase = _convertSequence(rawCase);

  // Skip the happy path
  if (workingCase.length === 0) {
    return 0;
  }

  // Recursively generate how many flips it takes
  const { flips } = _handleFlip(workingCase);
  return flips;
};

/**
 * Used to parse the sequence of plusses and minuses
 * @param {String} rawCase String of only + or -
 */
const _convertSequence = rawCase => {
  if (rawCase.split === undefined) {
    throw new Error(`Invalid case ${rawCase}`);
  }
  return rawCase.split("").map(el => {
    switch (el) {
      case "+":
        return true;
      case "-":
        return false;
      default:
        throw new Error(`Invalid case ${rawCase}`);
    }
  });
};

/**
 * Check if a case needs to be flipped. Returns true if it needs a flip, false if it's good to go
 * @param {Array} flipCase Array of boolean values
 */
const _needsFlip = flipCase => flipCase.indexOf(false) > -1;

/**
 * Recursive function to walk through a sequence and turn it into
 * @param {Array} flipCase Array of booleans indicating if an element is face up (true) or face down (false)
 * @param {Integer} flips Counts the number of times we've flipped
 */
const _handleFlip = (flipCase, flips = 0) => {
  if (!_needsFlip(flipCase)) {
    // If we're done, we're done!
    return { flipCase, flips };
  }

  // Extract next partial
  const flipIndex = _getFlipPartialIndex(flipCase);
  const toFlip = flipCase.slice(0, flipIndex);
  const toLeave = flipCase.slice(flipIndex, flipCase.length);

  // Flip the partial and concat it with the remainder
  const newCase = _flipPartial(toFlip).concat(toLeave);

  // Return the new array and increment our flips
  return _handleFlip(newCase, flips + 1);
};

/**
 * Returns the index of partial sequence to flip
 * @param {Array} seq Array of booleans representing our stack
 */
const _getFlipPartialIndex = seq => {
  if (!_needsFlip(seq)) {
    return 0;
  } else if (seq.indexOf(!seq[0]) > -1) {
    return seq.indexOf(!seq[0]);
  } else {
    return seq.length;
  }
};

/**
 * Flips the sign of each element in the partial and then reverses the stack (since the stack is now upside down)
 * @param {Array} seq Array of booleans representing our stack
 */
const _flipPartial = seq => seq.map(el => !el).reverse();

module.exports = {
  flipper,
  _needsFlip,
  _handleFlip,
  _flipPartial,
  _getFlipPartialIndex,
  _convertSequence
};

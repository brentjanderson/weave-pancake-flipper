const {
  flipper,
  _needsFlip,
  _handleFlip,
  _flipPartial,
  _getFlipPartialIndex,
  _convertSequence
} = require("../src/flipper");

describe("flipper", () => {
  test("-", () => {
    expect(flipper("-")).toBe(1);
  });

  test("-+", () => {
    expect(flipper("-+")).toBe(1);
  });

  test("+-", () => {
    expect(flipper("+-")).toBe(2);
  });

  test("+++", () => {
    expect(flipper("+++")).toBe(0);
  });

  test("--+-", () => {
    expect(flipper("--+-")).toBe(3);
  });

  test("", () => {
    expect(flipper("")).toBe(0);
  });
});

describe("_needsFlip", () => {
  test("returns true if we have false elements", () => {
    expect(_needsFlip([false, true, false, false])).toBe(true);
  });
  test("returns false if all true elements", () => {
    expect(_needsFlip([true, true, true, true])).toBe(false);
  });
});

describe("_convertSequence", () => {
  test("-", () => {
    expect(_convertSequence("-")).toStrictEqual([false]);
  });

  test("-+", () => {
    expect(_convertSequence("-+")).toStrictEqual([false, true]);
  });

  test("+-", () => {
    expect(_convertSequence("+-")).toStrictEqual([true, false]);
  });

  test("+++", () => {
    expect(_convertSequence("+++")).toStrictEqual([true, true, true]);
  });

  test("--+-", () => {
    expect(_convertSequence("--+-")).toStrictEqual([false, false, true, false]);
  });

  test("rejects bad data", () => {
    expect(() => _convertSequence(1234)).toThrow();
    expect(() => _convertSequence("+-_")).toThrow();
  });
});

describe("_getFlipPartialIndex", () => {
  test("-", () => {
    expect(_getFlipPartialIndex([false])).toBe(1);
  });

  test("-+", () => {
    expect(_getFlipPartialIndex([false, true])).toBe(1);
  });

  test("+-", () => {
    expect(_getFlipPartialIndex([true, false])).toBe(1);
  });

  test("+++", () => {
    expect(_getFlipPartialIndex([true, true, true])).toBe(0);
  });

  test("--+-", () => {
    expect(_getFlipPartialIndex([false, false, true, false])).toBe(2);
  });
});

describe("_flipPartial", () => {
  test("inverts the series correctly", () => {
    expect(_flipPartial([false, false, true])).toStrictEqual([
      false,
      true,
      true
    ]);
    expect(_flipPartial([false])).toStrictEqual([true]);
    expect(_flipPartial([true])).toStrictEqual([false]);
    expect(_flipPartial([false, true])).toStrictEqual([false, true]);
    expect(_flipPartial([true, true, true])).toStrictEqual([
      false,
      false,
      false
    ]);
    expect(_flipPartial([false, false, true, false])).toStrictEqual([
      true,
      false,
      true,
      true
    ]);
  });
});

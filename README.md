# Infinite House of Pancakes

This was a fun code challenge. I approached it as follows:

1. TDD - I started by installing Jest before even creating any files.
   - I didn't set up tests for the index.js file, since that's concerned more with capturing user input and formatting it for output.
2. I decided to use a recursive function to handle this. I also tried to avoid loops as much as possible, and rely heavily on `Array.indexOf` to hopefully run as efficiently as possible.
3. I started with the `flipper` function, and broke out functionality into tiny, pure functions.
4. Each time I decided to break out a new function, I wrote test cases and did a red-green-refactor cycle to get the function up to par.
5. Once I got the test cases green, I wrote the `index.js` file.
   - I searched online for best practices with NodeJS input capture, decided a full dependency was overkill, and pulled up the NodeJS docs for `readline`.
   - Apart from exploring the docs for `Array.reduce` on MDN a bit to see if there's a way to break out of a reduce (there isn't, as near as I could tell), and the aforementioned research that led me to the `readline` docs, I surprisingly didn't need any other web searches to produce this solution.
6. I did a few manual tests, extended the test coverage to 100% coverage for fun, and made sure everything was tidy.

Overall I'm pretty proud of the solution, and am happy to answer any questions you like about it.

## How to run it

1. `yarn`
2. `yarn start`
3. `yarn test # If you want to check out the test suite`
